package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class Main {


    public static void main(String[] args) {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 31) - 15);
        }
        List<Integer> list = new ArrayList<>();
        for (int list1 : array) {
            list.add(list1);
        }

        System.out.println("Генератор рандомних чисел : " + list);

        list.stream().map(a -> a * a).forEach(System.out::println);
        int sum = list.stream().map(a -> a * a).reduce(0, (a, b) -> a + b);
        System.out.println("Сума квадратів : " + sum);

        // Tusk number : 1

        long total = list.stream().filter((i) -> i % 2 == 0).count();
        System.out.println(total);


        // Tusk number : 2


        String[] surnamesArray = {"Adamson","Smith","Johnson","Williams","Brown","Jones","Walker","KiJka"};
        List<String>surnames = new ArrayList<>();
        for (String surnames1 : surnamesArray){
            surnames.add(surnames1);
        }
        System.out.println("Фамілії : " + surnames);
        surnames.stream().filter(a -> a.startsWith("J")).forEach(System.out::println);

        // Tusk number 3





    }

}